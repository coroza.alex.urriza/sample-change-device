import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectMediaPageComponent } from './pages/select-media-page/select-media-page.component';



const routes: Routes = [
	{ path: '', redirectTo: 'select-media-page', pathMatch: 'full' },
	{ path: 'select-media-page', component: SelectMediaPageComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
