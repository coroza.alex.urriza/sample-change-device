import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMediaPageComponent } from './select-media-page.component';

describe('SelectMediaPageComponent', () => {
  let component: SelectMediaPageComponent;
  let fixture: ComponentFixture<SelectMediaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMediaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMediaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
