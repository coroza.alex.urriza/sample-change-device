import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-select-media-page',
	templateUrl: './select-media-page.component.html',
	styleUrls: ['./select-media-page.component.scss']
})
export class SelectMediaPageComponent implements OnInit {

	mediaDevices: MediaDeviceInfo[];
	mediaStream: MediaStream;
	selectedMicrophoneLabel: string;
	selectedCameraLabel: string;
	
	
	constructor() { }

	async ngOnInit() {
		this.mediaStream = await this.getDeviceStream();
		this.mediaDevices = await this.getMediaDevices();
		this.defaultDevices(this.mediaStream);
		this.initVideo(this.mediaStream);

		console.log(this.mediaDevices);
	}



	/**
	 * Get the list of available media devices such as camera and microphone
	 */
	async getMediaDevices(): Promise<MediaDeviceInfo[]> {
		return navigator.mediaDevices.enumerateDevices();
	}



	/**
	 * Gets and sets the default microphone and camera devices
	 */
	defaultDevices(mediaStream: MediaStream) : void {
		let mediaStreamTracks: MediaStreamTrack[] = mediaStream.getTracks();
		mediaStreamTracks.forEach((mediaStreamTrack) => {
			if(mediaStreamTrack.kind === 'audio') {
				this.selectedMicrophoneLabel = mediaStreamTrack.label;
			}

			if(mediaStreamTrack.kind === 'video') {
				this.selectedCameraLabel = mediaStreamTrack.label;
			}
		});
	}



	/**
	 * Return a media stream
	 * And get client browser's camera and audio media stream at the same time
	 */
	async getDeviceStream(): Promise<MediaStream> {
		return await navigator.mediaDevices.getUserMedia({
			audio: true,
			video: true
		});
	}



	/**
	 * Feed video/audio stream to video#video-output
	 * @param mediaStream MediaStream
	 */
	initVideo(mediaStream: MediaStream) : void {
		let video: HTMLVideoElement = document.querySelector('div#select-media-page video');
		video.srcObject = mediaStream; 
	}



	/**
	 * Get and set new video/audio stream by using the provided/selected audio/video devices
	 */
	async changeDevice(cameraDeviceLabel: string, microphoneDeviceLabel: string) {
		this.mediaStream.getTracks().forEach((track) => {
			track.stop();
		});
		
		let cameraDeviceId = this.getMediaDeviceIdByLabel(cameraDeviceLabel);
		let microphoneDeviceId = this.getMediaDeviceIdByLabel(microphoneDeviceLabel);
		
		let mediaStream = await navigator.mediaDevices.getUserMedia({
			audio: { deviceId: microphoneDeviceId },
			video: { deviceId: cameraDeviceId }
		});

		this.initVideo(mediaStream);
	}



	/**
	 * Get the corresponding deviceId from 
	 * @param mediaDeviceLabel device's label(because MediaStream.getTracks doesnt return deviceId but returns device label)
	 */
	getMediaDeviceIdByLabel(mediaDeviceLabel: string): string {
		let deviceId = ''
		
		this.mediaDevices.forEach((mediaDevice) => {
			if(mediaDevice.label === mediaDeviceLabel) {
				deviceId = mediaDevice.deviceId;
			}
		});

		return deviceId;
	}



}
